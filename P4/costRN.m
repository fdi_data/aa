# CostRN calculates the cost and the gradient of a neural network of two layers
function [J, grad] = costRN (params_rn, num_inputs, num_hidden, num_labels, X, y,lambda)
warning ("off");

m = length(X(:,1));

# Reshape params_rn back into the the weight matrices for our 2-layer neural network
Theta1 = reshape (params_rn (1:num_hidden * (num_inputs + 1)), num_hidden, (num_inputs + 1));

Theta2 = reshape (params_rn ((1 + ( num_hidden * (num_inputs + 1))): end ), num_labels ,( num_hidden + 1 ));

# Initialize the variables
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

#Forward Propagation

a1 = [ones(rows(X), 1), X];
z2 = (Theta1 * a1');
a2 = sigmoidFunction(z2);

a2 = [ones(1, columns(a2)); a2];
z3 = Theta2 * a2;
a3 = sigmoidFunction(z3);

#Initializes and gives values to the yk matrix (num_labels * m)

yk = zeros(num_labels, m);
for i= 1 :m
  yk(:,i) = (y(i)==[1:num_labels]');
end

#Calculate the cost

J = (1 / m) * sum(sum((-1 * yk).*(log(a3)) - (1-yk).*(log(1-a3))));

# Regulization of the cost

Theta1_trim = Theta1(:, 2:columns(Theta1));
Theta2_trim = Theta2(:, 2:columns(Theta2));

regulation_theta1 = sum(sum(Theta1_trim.*Theta1_trim));
regulation_theta2 = sum(sum(Theta2_trim.*Theta2_trim));

J = J + (regulation_theta1 + regulation_theta2)*lambda/(2*m);

#Backpropagation

d3 = (a3-yk);
#d2 = a2.*(1-a2).*(Theta2'*d3);
d2 = (Theta2'*d3) .* sigmoidDerivative(z2);

D2 = zeros(size(Theta2));
D1 = zeros(size(Theta1));

#Backpropagation algorithm
for i=1:m

	D2 = D2 + d3(:,i)*(a2(:,i))';

  d2mod = d2(:,i);
  d2mod = d2mod(2:size(d2,1));
  D1 = D1 + d2mod * (a1(i,:));

end

# Regularization of the gradient

Theta1_grad = (D1./m) + ((lambda/m) .* [zeros( num_hidden, 1 ), Theta1(:,2:columns(Theta1))]);
Theta2_grad = (D2./m) + ((lambda/m) .* [zeros( num_labels, 1 ), Theta2(:,2:columns(Theta2))]);

# Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];

endfunction
