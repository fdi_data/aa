# Stores the data in X,y (5000 x 400)
load("ex4data1.mat");

#Parameters of the 2-layer neural network

num_inputs = columns(X);
num_hidden = 25;
num_labels = 10;
lambda = 1;

#Training of the neural network
#[Theta1, Theta2] = nnTraining (X,y,num_inputs, num_hidden, num_labels, lambda);

#Gradient checking
#checkNNGradients(lambda);

#Save trained thetas in memory
#dlmwrite("cachedTheta1", Theta1);
#dlmwrite("cachedTheta2", Theta2);

#Load cached thetas
Theta1 = dlmread("cachedTheta1");
Theta2 = dlmread("cachedTheta2");

hypothesis = forwardPropagation(X,Theta1,Theta2);
display(["Neural network accuracy is: " num2str(percentageAccuracy(hypothesis, y)) "%"]);
