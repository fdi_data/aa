function [Theta1, Theta2] = nnTraining (X,y,num_inputs, num_hidden, num_labels, lambda)

# Loads the trained weight matrix (TO CHECK IF THE COST IT'S RIGHT)
#load("ex4weights.mat");
#Theta1 has a 25 x 401 dimension
#Theta2 has a 10 x 26 dimension

#Initialize theta matrices with random values
Theta1 = randomWeights (num_hidden,num_inputs);
Theta2 = randomWeights (num_labels,num_hidden);

#Roll the matrices in one initial vector
initial_params_rn = [Theta1(:); Theta2(:)];

options = optimset("GradObj", "on", "MaxIter", 200);
[params_rn] = fmincg (@(t)(costRN(t , num_inputs, num_hidden,num_labels, X, y , lambda)) , initial_params_rn , options);

#Unroll the resulting theta vector into matrices

Theta1 = reshape (params_rn (1:num_hidden * (num_inputs + 1)), num_hidden, (num_inputs + 1));

Theta2 = reshape (params_rn ((1 + ( num_hidden * (num_inputs + 1))): end ), num_labels ,( num_hidden + 1 ));

endfunction