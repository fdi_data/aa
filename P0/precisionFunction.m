function [] = precisionFunction(fun, a, b, maximumPrecision);
	x = [1:1:maximumPrecision];
	y = [1:1:maximumPrecision];
	for i=1:maximumPrecision
		y(i) = mcint(fun,a,b,i);
	endfor

	#Draw the graphics
	plot(x,y,"linewidth",2);
endfunction

