# Calculates the approx. integral of the function x^2 + 1 for
# the interval [-1,1] and evaluating 20 points

fun = @(x)(-(x.^2) + 1);
num_puntos = 20;
a = -1;
b = 1;
mcint(fun,a,b,num_puntos);

disp("\nPress enter to continue...\n");                                
pause();

#...evaluating 200 points
num_puntos = 200;
mcint(fun,a,b,num_puntos);

disp("\nPress enter to continue...\n");
pause();

#...evaluating 2000 points
num_puntos = 2000;
mcint(fun,a,b,num_puntos);

disp("\nPress enter to continue...\n");
pause();



