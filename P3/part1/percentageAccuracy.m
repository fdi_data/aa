function percentageHits = percentageAccuracy(X, y, theta)
	prediction = recognition(X, theta);
	m = length(y);

	# Whenever the expected value and the real value are the same is a hit
	hits = sum(prediction == y');
	percentageHits = hits/m * 100;
endfunction
