function [J,grad] = costFunction (theta,X,y,lambda)
warning("off");

m = rows(X);
n = columns(X);

#Cost computation

J = (1 / (2*m)) * sum((hFunction(X,theta)-y).^2);
JReg = (lambda/(2 * m)) * sum(theta(2:n) .^ 2);
J = J + JReg;

#Gradient computation

grad = (1 / m) .* sum((hFunction(X,theta) - y) .* X);
gradReg = [0;lambda/m .* theta(2:n,:)]';
grad = (grad + gradReg);
grad = grad';


endfunction
