function error = checkError(testX,testY,theta,mu,sigma,p)
m = rows(testX);
extX = featureExtension(testX,p);
extX = bsxfun(@minus, extX, mu);
extX = bsxfun(@rdivide, extX, sigma);
extX = [ones(m,1),extX];
error =  costFunction(theta,extX,testY,0);

endfunction
