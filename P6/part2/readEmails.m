function [X, y, emailNum] = readEmails(amount, directory, yValue, vocabHash, emailNum, X, y)
	warning("off");
	for i=1:amount
		fileName = sprintf("%s/%04d.txt", directory, i);

		if (mod((i/amount) * 100,10)	== 0)
			fprintf("....%d%%",(i/amount) * 100);
			fflush(stdout);
		endif
		file_contents = readFile(fileName);
		email = processEmail(file_contents);

		while !isempty(email)
			[str, email] = strtok(email, [' ']);

			if isfield(vocabHash, str)
				#disp(["Word was in the Hash, word was: " str]);
				X(emailNum, vocabHash.(str)) = 1;
			else
				#disp(["Word wasn't in the Hash, word was: " str]);
			endif

		endwhile
		y(emailNum++,:) = yValue;
	endfor

fprintf("\n",fileName);
endfunction
