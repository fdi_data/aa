function percentageHits = percentageAccuracy(hypothesis, y)
	prediction = hypothesis;
	m = length(y);

	# A hit is a coincidence between a expected value and the real value
	hits = sum(prediction == y);
	percentageHits = hits/m * 100;
endfunction
