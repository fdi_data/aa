function [result] = costFunction (theta,X,y)

m = length(y);
X = [ones(m,1),X];

#Method 1
#result = (1 / (2*m)) * sum((hFunction(X,theta,m)-y).^2);

#Method 2
result = (1 / (2*m)) * (X * theta - y)' * (X * theta - y);
endfunction
