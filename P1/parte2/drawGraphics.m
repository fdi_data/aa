function void = drawGraphics(xVector, yVector)
	alphaVector = [.03, .01, .003, .001, .0003, .0001];
	colorVector = ["b", "c", "g", "y", "r", "m"];

	for(i = 1:length(alphaVector))
		[useless, thetas] = descentGradient(xVector, yVector, alphaVector(i));
		for(j = 1:length(thetas))
			costVector(j) = costFunction(thetas(:,j), xVector, yVector);
		endfor
		plot(costVector, "linewidth", 2, "color", colorVector(i));
		hold on;
	endfor

	title("Cost against alpha", "fontsize", 25);
	ylabel("Cost", "fontsize", 15);
	xlabel("Step number", "fontsize", 15);
	text(1500,0.07e+10, "alpha = .03", "fontsize", 15, "color", "b");
	text(1500,0.27e+10, "alpha = .01", "fontsize", 15, "color", "c");
	text(1500,0.57e+10, "alpha = .003", "fontsize", 15, "color", "g");
	text(1500,0.87e+10, "alpha = .001", "fontsize", 15, "color", "y");
	text(1500,3.07e+10, "alpha = .0003", "fontsize", 15, "color", "r");
	text(1500,5.07e+10, "alpha = .0001", "fontsize", 15, "color", "m");
endfunction
