function [J,grad] = costFunction (theta,X,y,lambda)

#Disable warnings
warning ("off");

m = length(y);
n = length(X(1,:)); 
J = ((1 / m) * sum(-y .* log(hFunction(X,theta)) - (1 - y) .* log (1 - hFunction(X,theta)))); 
regularizationTerm1 = (lambda/(2 * m)) * sum(theta .^ 2);

J = J + regularizationTerm1;

grad = (1 / m) .* sum((hFunction(X,theta) - y) .* X);

regularizationTerm2 = [0;lambda/m .* theta(2:n,:)];

grad = grad + regularizationTerm2';

endfunction
