function [result] = hFunction (X,theta)
z = theta' * X';
result = sigmoidFunction(z)';
endfunction
