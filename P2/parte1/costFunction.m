function [J,grad] = costFunction (theta,X,y)

#Disable warnings
warning ("off");

m = length(y);
J = (1 / m) * sum(-y .* log(hFunction(X,theta)) - (1 - y) .* log (1 - hFunction(X,theta)));

grad = (1 / m) .* sum((hFunction(X,theta) - y) .* X);

endfunction
