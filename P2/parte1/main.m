data = load ("ex2data1.txt");
n = length(data(1,:))-1;
X = data(:,1:n);
y = data(:,n+1); 
m = length(y);
X = [ones(m,1),X];
initial_theta = zeros(n+1,1);

#Optimization
options = optimset('GradObj','on','MaxIter',400);
[theta,cost] = fminunc(@(t)(costFunction(t,X,y)), initial_theta,options)

#Graphics
plotDecisionBoundary(theta,X,y);

#Percentage of hits
printf("Percentage of hits: %i %% \n" ,evaluation(theta,X,y));

